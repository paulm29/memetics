package au.com.memetics.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Getter
@Setter
@Entity
@EqualsAndHashCode
@Table(name = "follow")
@ApiModel(value = "follow")
@NoArgsConstructor
public class Follow {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "follow_id")
    private Long id;

    @JsonIgnoreProperties(value = {"followers", "following"}, allowSetters = true)
    @ManyToOne
    @JoinColumn(name = "follower_id", nullable = false)
//    @NotFound(action = NotFoundAction.IGNORE)
    private Profile follower;

    @JsonIgnoreProperties(value = {"followers", "following"}, allowSetters = true)
    @ManyToOne
    @JoinColumn(name = "following_id", nullable = false)
//    @NotFound(action = NotFoundAction.IGNORE)
    private Profile following;

    @Version
    private long version;
}
