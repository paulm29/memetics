package au.com.memetics.entity;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ApiModel(value = "MemeTweet")
public class MemeTweet {
    private Long memeId;
    private Long profileId;
    private String text;
    private String imageUrl;
    private boolean textOnly = false;
}
