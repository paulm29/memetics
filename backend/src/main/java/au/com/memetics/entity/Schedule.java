package au.com.memetics.entity;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

import java.time.LocalTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Entity
@Table(name = "schedule")
@ApiModel(value = "Schedule")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Slf4j
public class Schedule {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "schedule_id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "profile_id", nullable = false)
    private Profile profile;

    @Column(name = "created_date")
    private Date createdDate;

    @Column(name = "modified_date")
    private Date modifiedDate;

    @ElementCollection(targetClass = String.class, fetch = EAGER)
    @CollectionTable(name = "schedule_day", joinColumns = @JoinColumn(name = "schedule_id"))
    @Column(name = "day", nullable = false)
    private Set<String> days = new HashSet<>();

    @ElementCollection(targetClass = LocalTime.class, fetch = EAGER)
    @CollectionTable(name = "schedule_time", joinColumns = @JoinColumn(name = "schedule_id"))
    @Column(name = "time", nullable = false)
    private Set<LocalTime> times = new HashSet<>();

    public boolean isTwitter() {
        return profile.isTwitter();
    }

    public boolean hasDay(String day) {
        for (String d : days) {
            if (day.equalsIgnoreCase(d)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasTimeWithinRange(LocalTime lastTime, LocalTime currentTime) {
        return times.stream().anyMatch(time -> {
            log.info("Time " + time + " is after lastTime " + lastTime + " : " + time.isAfter(lastTime));
            log.info("Time " + time + " is before currentTime " + currentTime + " : " + time.isBefore(currentTime));
            return time.isAfter(lastTime) && (time.equals(currentTime) || time.isBefore(currentTime));
        });
    }

    @Version
    private long version;

    @PrePersist
    public void prePersist() {
        this.createdDate = new Date();
        this.modifiedDate = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        this.modifiedDate = new Date();
    }
}
