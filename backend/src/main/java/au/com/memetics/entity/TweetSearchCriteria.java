package au.com.memetics.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ApiModel(value = "TweetSearchCriteria")
public class TweetSearchCriteria {
    @ApiModelProperty("Minimum date/time of tweets to return")
    public String startDate;

    @ApiModelProperty("Maximum date/time of tweets to return")
    public String endDate;

    @ApiModelProperty("How many tweets to return.")
    public Integer count;

    public boolean notRetweeted;
}
