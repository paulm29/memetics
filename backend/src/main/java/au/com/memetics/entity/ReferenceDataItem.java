package au.com.memetics.entity;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
@ApiModel(value = "ReferenceDataItem")
public class ReferenceDataItem {
    private String code;
    private String description;
}
