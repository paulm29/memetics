package au.com.memetics.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.social.twitter.api.Tweet;

@Getter
@Setter
@Entity
@Table(name = "tweet_info")
@ToString
public class TweetInfo {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "tweet_info_id")
    private Long tweetInfoId;

    @Column(name = "profile_id")
    private long profileId;

    @Column(name = "meme_id")
    private long memeId;

    @Column(name = "status_id")
    private String statusId; // https://twitter.com/AussieMAGA/status/<id>

    @Column(name = "text")
    private String text;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "from_user")
    private String fromUser;

    @Column(name = "source")
    private String source; // web site from which the tweet was sent

    @Version
    private long version;
}
