package au.com.memetics.entity;

import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import au.com.memetics.dto.MemeExportDTO;

@Getter
@Setter
@ToString
@ApiModel(value = "CSV export")
public class CsvExport {
    private List<MemeExportDTO> exports;
    private String filename;
}
