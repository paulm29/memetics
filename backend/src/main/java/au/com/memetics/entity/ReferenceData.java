package au.com.memetics.entity;

import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@ApiModel(value = "ReferenceData")
public class ReferenceData {
    private List<ReferenceDataItem> indigenousStatus;
    private List<ReferenceDataItem> countries;
    private List<ReferenceDataItem> birthCountries;
    private List<ReferenceDataItem> languages;
    private List<ReferenceDataItem> australianStates;
    private List<ReferenceDataItem> genders;
}
