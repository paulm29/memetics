package au.com.memetics.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "ProfileSearchCriteria")
public class ProfileSearchCriteria {
    @ApiModelProperty("Matches profile nickname. Exact match only.")
    public String nickname;

    @ApiModelProperty("Matches profile email. Exact match only.")
    public String email;
}
