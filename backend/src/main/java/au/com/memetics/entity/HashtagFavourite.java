package au.com.memetics.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "hashtag_favourite")
@ApiModel(value = "hashtag favourite")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class HashtagFavourite {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "hashtag_favourite_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "profile_id")
    @JsonIgnoreProperties(value = {"hashtagFavourites", "followers", "following"})
    private Profile profile;

    @Column(name = "hashtag")
    private String hashtag;

    @Version
    private long version;
}
