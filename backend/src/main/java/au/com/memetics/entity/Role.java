package au.com.memetics.entity;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Role")
public enum Role {
    ROLE_USER, ROLE_ADMIN
}
