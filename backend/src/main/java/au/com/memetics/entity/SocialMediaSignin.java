package au.com.memetics.entity;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "SocialMediaSignin")
public enum SocialMediaSignin {
    FACEBOOK,
    TWITTER,
    NONE
}
