package au.com.memetics.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@SqlResultSetMapping(
    name = "tagStatsAggregated",
    classes = {
        @ConstructorResult(
            targetClass = TagStatAggregated.class,
            columns = {
                @ColumnResult(name = "category", type = String.class),
                @ColumnResult(name = "weight", type = Long.class)
        })
    })
@ApiModel(value = "tagStat")
@NoArgsConstructor
public class TagStatAggregated {
    @Id
    @Column(name = "category")
    private String category;

    @Column(name = "weight")
    private long weight;

    public TagStatAggregated(final String category, final long weight) {
        this.category = category;
        this.weight = weight;
    }
}
