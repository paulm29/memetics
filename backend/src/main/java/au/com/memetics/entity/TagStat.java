package au.com.memetics.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@SqlResultSetMapping(
    name = "tagStats",
    classes = {
        @ConstructorResult(
            targetClass = TagStat.class,
            columns = {
                @ColumnResult(name = "tagId", type = Long.class),
                @ColumnResult(name = "text", type = String.class),
                @ColumnResult(name = "weight", type = Long.class)
        })
    })
@ApiModel(value = "tagStat")
@NoArgsConstructor
public class TagStat {
    @Id
    @Column(name = "tagId")
    @ApiModelProperty(readOnly = true)
    private Long tagId;

    @Column(name = "text")
    private String text;

    @Column(name = "weight")
    private long weight;

    public TagStat(final Long tagId, final String text, final long weight) {
        this.tagId = tagId;
        this.text = text;
        this.weight = weight;
    }
}
