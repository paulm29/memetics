package au.com.memetics.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Getter
@Setter
@ToString
@ApiModel(value = "MemeSearchCriteria")
public class MemeSearchCriteria {
    @ApiModelProperty("Maximum results to return.  Defaults to 1000.")
    private Integer maxResults = 1000;

    @ApiModelProperty("Matches user's profile ID. Exact match only.")
    private boolean myMemes;

    @ApiModelProperty("Used with myMemes. Exact match only.")
    private long profileId;

    @ApiModelProperty("Matches profile nickname. Exact match only.")
    private String nickname;

    @ApiModelProperty("Matches title of meme. Case insensitive.")
    private String title;

    @ApiModelProperty("Matches credits for meme. Case insensitive.")
    private String credits;

    @ApiModelProperty(value = "Comma separated list of tags. Matches memes with any of the tags.")
    private String tags;

    @JsonIgnore
    @ApiIgnore
    public List<String> getTagList() {
        List<String> tagList = new ArrayList<>();
        if (isNotBlank(tags)) {
            tagList = Arrays.asList(tags.split(","));
        }
        return tagList;
    }

    @ApiModelProperty(value = "Only matches when provided criteria is an exact match. Case insensitive. Applies only to string fields.")
    private boolean exactMatch = false;
}
