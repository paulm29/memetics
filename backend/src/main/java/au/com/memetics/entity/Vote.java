package au.com.memetics.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@Entity
@Table(name = "vote")
@ApiModel(value = "Vote")
@NoArgsConstructor
public class Vote {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "vote_id")
    @ApiModelProperty(readOnly = true)
    private Long id;

//    @ManyToOne
//    @JoinColumn(name = "meme_id")
//    @JsonIgnoreProperties(value = {"votes", "comments"}, allowSetters = true)
//    private Meme meme;

    @Column(name = "meme_id")
    private Long memeId;

    @Column(name = "profile_id")
    private Long profileId;

    @Column(name = "score")
    private Long score;

    @Version
    private long version;
}
