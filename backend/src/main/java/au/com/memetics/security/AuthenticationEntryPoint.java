package au.com.memetics.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AuthenticationEntryPoint implements org.springframework.security.web.AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {

        log.info("================================================");
        if (request.getUserPrincipal() != null) {
            log.debug("commence: " + request.getUserPrincipal().getName());
        }
        log.info("================================================");


        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
}