package au.com.memetics.service;


import au.com.memetics.dto.ReferenceDataDTO;
import au.com.memetics.dto.ReferenceDataItem;
import au.com.memetics.mapping.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReferenceDataService {
    private final Mapper mapper;

    @Autowired
    public ReferenceDataService(Mapper mapper) {
        this.mapper = mapper;
    }

    public ReferenceDataDTO get() {
        ReferenceDataDTO referenceData = new ReferenceDataDTO();

        List<ReferenceDataItem> states = new ArrayList<>();
        states.add(item("QLD", "Queensland"));
        states.add(item("NSW", "New South Wales"));
        states.add(item("VIC", "Victoria"));
        states.add(item("SA", "South Australia"));
        states.add(item("WA", "Western Australia"));
        states.add(item("NT", "Northern Territory"));
        states.add(item("ACT", "Australian Capital Territory"));
        states.add(item("TAS", "Tasmania"));
        referenceData.setStates(states);


        List<ReferenceDataItem> countries = new ArrayList<>();
        countries.add(item("AU", "Australia"));
        referenceData.setCountries(countries);


        return referenceData;
    }

    private ReferenceDataItem item(String code, String description) {
        return new ReferenceDataItem(code, description);
    }
}
