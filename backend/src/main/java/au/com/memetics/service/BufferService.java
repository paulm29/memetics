package au.com.memetics.service;

import au.com.memetics.dto.BufferRetweetDTO;
import au.com.memetics.entity.BufferRetweet;
import au.com.memetics.entity.BufferUpdate;
import com.bufferj.client.BufferJ;
import com.bufferj.client.BufferJException;
import com.bufferj.client.util.CreateUpdates;
import com.bufferj.entity.Profile;
import com.bufferj.entity.Update;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
public class BufferService {
    @Value("${bufferAccessToken}")
    private String bufferAccessToken;

    @Value("${bufferTwitterProfileId}")
    private String bufferTwitterProfileId;

    public List<Update> retweetCreate(BufferRetweetDTO dto) {
        BufferRetweet entity = new BufferRetweet();
        entity.getRetweet().put("tweet_id", dto.getTweetId());
        entity.getRetweet().put("comment", dto.getComment());
        entity.setTop(dto.isTop());

        BufferJ bufferJ = getBuffer();

        CreateUpdates createUpdates = new CreateUpdates();
        createUpdates.addProfile(getTwitterBufferProfile(bufferJ));
        createUpdates.setRetweet(entity.getRetweet());
        createUpdates.setTop(entity.isTop());

        List<Update> updatesCreated; // only one update will be created since we only added one media profile
        try {
            updatesCreated = bufferJ.create(createUpdates);
        } catch (IOException | BufferJException e) {
            throw new RuntimeException(e);
        }
        return updatesCreated;
    }

    public List<Update> bufferUpdateCreate(BufferUpdate bufferUpdate) {
        CreateUpdates createUpdates = new CreateUpdates();
        createUpdates.setText(bufferUpdate.getText());

        BufferJ bufferJ = getBuffer();
        for (String service : bufferUpdate.getServices()) {
            createUpdates.addProfile(getBufferProfile(bufferJ, service));
            if (Objects.equals(com.bufferj.client.util.Service.TWITTER, service)) {
                // TODO resolve twitter t.co URL if possible
                // AUSTRALIAN DIARY: Everything you need to know about the week ahead for markets https://www.businessinsider.com.au/australian-diary-everything-you-need-to-know-about-the-week-ahead-for-markets-v3-2018-7
            }
        }

        List<Update> updatesCreated;
        try {
            return bufferJ.create(createUpdates);
        } catch (IOException | BufferJException e) {
            throw new RuntimeException(e);
        }
    }

    private com.bufferj.entity.Profile getBufferProfile(BufferJ bufferJ, String service) {
        try {
            return bufferJ.getProfile(com.bufferj.client.util.Service.getService(service));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Profile getTwitterBufferProfile(BufferJ bufferJ) {
        try {
            return bufferJ.getProfile(bufferTwitterProfileId);
        } catch (IOException | BufferJException e) {
            throw new RuntimeException(e);
        }
    }

    private BufferJ getBuffer() {
        try {
            return new BufferJ(bufferAccessToken);
        } catch (IOException | BufferJException e) {
            throw new RuntimeException(e);
        }
    }
}
