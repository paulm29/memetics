package au.com.memetics;


import au.com.memetics.dao.UserRepository;
import au.com.memetics.security.AuthenticationEntryPoint;
import au.com.memetics.security.AuthenticationSuccessHandler;
import au.com.memetics.security.LogoutSuccessHandler;
import au.com.memetics.service.FollowService;
import au.com.memetics.service.ProfileServiceImpl;
import au.com.memetics.service.SocialUserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.social.security.SpringSocialConfigurer;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Profile(value = {"dev", "ci", "production", "test"})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FollowService followService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${security.enabled:true}")
    private boolean securityEnabled;


    private final AuthenticationEntryPoint authenticationEntryPoint;
    private final AuthenticationSuccessHandler authenticationSuccessHandler;
    private final LogoutSuccessHandler logoutSuccessHandler;

    @Autowired
    public WebSecurityConfig(AuthenticationEntryPoint authenticationEntryPoint, AuthenticationSuccessHandler authenticationSuccessHandler, LogoutSuccessHandler logoutSuccessHandler) {
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.logoutSuccessHandler = logoutSuccessHandler;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/rest/admin/**").hasRole("ADMIN")
                .antMatchers("/rest/profiles/search").permitAll() // duplicate check
                .antMatchers(
                        "/auth/**",
                        "/login",                   // twitter ?
                        "/logout",                  // twitter ?
                        "/signin",                  // facebook
                        "/signup",                  // twitter ?
                        "/register",                // twitter ?
                        "/registration",            // twitter ?
                        "/rest/registration",       // registrationGetAll - TODO have separate email check endpoint instead?
                        "/rest/application-config",  // application config
                        "/rest/memes/**",  // only for e2e tests
                        "/rest/profiles/**",  // only for e2e tests
                        "/rest/queue"  // only for e2e tests
                ).permitAll()
                .antMatchers("/rest/**").authenticated()
                .antMatchers("/**").permitAll() // should be redundant given above, but haven't bothered testing
            .and()
                .apply(new SpringSocialConfigurer());

        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
        http.formLogin()
                .successHandler(authenticationSuccessHandler)
                .failureHandler(new SimpleUrlAuthenticationFailureHandler());
        http.logout().logoutSuccessHandler(logoutSuccessHandler);

        http.csrf().disable();
        http.headers().cacheControl();
        http.headers().frameOptions().disable(); // needed to use H2 web console
    }

    @Override
    public void configure(final WebSecurity web) {
        if (securityEnabled) {
            web.ignoring().antMatchers("/static/**");
        } else {
            web.ignoring().antMatchers("/**");
        }
    }

    /**
     * Configures the authentication manager bean which processes authentication
     * requests.
     */
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder);
    }

    /**
     * This bean is used to load the user specific data when social sign in
     * is used.
     */
    @Bean
    public SocialUserDetailsService socialUserDetailsService() {
        return new SocialUserDetailsServiceImpl(userDetailsService());
    }

    /**
     * This bean is load the user specific data when form login is used.
     */
    @Bean
    public UserDetailsService userDetailsService() {
        return new ProfileServiceImpl(userRepository, followService);
    }
}
