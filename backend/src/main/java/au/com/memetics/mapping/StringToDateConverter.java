package au.com.memetics.mapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

@Component
public class StringToDateConverter extends BidirectionalConverter<String, Date> {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

   @Override
    public Date convertTo(String s, Type<Date> type, MappingContext mappingContext) {
        if (s == null) {
            return null;
        }

        try {
            return java.sql.Date.valueOf(LocalDate.parse(s, formatter));
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    @Override
    public String convertFrom(Date date, Type<String> type, MappingContext mappingContext) {
        if (date == null) {
            return null;
        }
        return formatter.format(new java.sql.Date(date.getTime()).toLocalDate());
    }
}
