package au.com.memetics.mapping;

import au.com.memetics.dto.ReferenceDataItem;
import com.afrozaar.wordpress.wpapi.v2.model.Term;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import au.com.memetics.controller.formsignin.Registration;
import au.com.memetics.dto.MemeExportDTO;
import au.com.memetics.dto.ProfileDTO;
import au.com.memetics.entity.Meme;
import au.com.memetics.entity.MemeTweet;
import au.com.memetics.entity.Profile;
import au.com.memetics.entity.QueueItem;
import au.com.memetics.entity.TweetInfo;

@Component
public class Mapper extends ConfigurableMapper {
    private final StringToDateConverter stringToDateConverter;

    @Autowired
    public Mapper(StringToDateConverter stringToDateConverter) {
        super(false);
        this.stringToDateConverter = stringToDateConverter;
        init();
    }

    @Override
    protected void configure(final MapperFactory factory) {

        factory.classMap(Profile.class, ProfileDTO.class)
                .fieldAToB("id", "id")
                .exclude("followers")
                .exclude("following")
                .exclude("hashtagFavourites")
                .byDefault()
                .register();

        factory.classMap(Meme.class, MemeExportDTO.class)
                .fieldAToB("id", "id")
                .fieldAToB("profile.nickname", "nickname")
                .fieldAToB("profile.nickname", "nickname")
                .exclude("tags")
                .byDefault()
                .register();

        factory.classMap(Registration.class, Profile.class)
                .exclude("socialMediaSignin")
                .byDefault()
                .register();

        factory.classMap(Tweet.class, TweetInfo.class)
                .fieldAToB("id", "statusId")
                .byDefault()
                .register();

        factory.classMap(QueueItem.class, MemeTweet.class)
                .fieldAToB("meme.id", "memeId")
                .fieldAToB("profile.id", "profileId")
                .fieldAToB("meme.url", "imageUrl")
                .byDefault()
                .register();

        factory.classMap(Term.class, ReferenceDataItem.class)
                .fieldAToB("id", "code")
                .fieldAToB("description", "description")
                .byDefault()
                .register();

        factory.getConverterFactory().registerConverter(stringToDateConverter);
    }
}
