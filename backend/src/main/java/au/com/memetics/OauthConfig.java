package au.com.memetics;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;

import java.util.ArrayList;
import java.util.List;

// https://stackoverflow.com/questions/27864295/how-to-use-oauth2resttemplate

//@Configuration
//@EnableOAuth2Client
public class OauthConfig {

    // http://www.baeldung.com/spring-security-5-oauth2-login
    // https://stackoverflow.com/questions/29696671/how-to-overwrite-spring-cloud-oauth2-client-autoconfiguration?rq=1
//    https://buffer.com/developers/api/oauth
    @Value("${bufferBaseUrl}")
    private String bufferBaseUrl;
    @Value("${bufferAuthorizeUrl}")
    private String bufferAuthorizeUrl;
    @Value("${bufferTokenUrl}")
    private String bufferTokenUrl;

    @Value("${bufferClientId}")
    private String bufferClientId;
    @Value("${bufferClientSecret}")
    private String bufferClientSecret;

    @Value("${bufferUsername}")
    private String bufferUsername;
    @Value("${bufferPassword}")
    private String bufferPassword;

    @Bean
    protected OAuth2ProtectedResourceDetails resource() {

        ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();

        List scopes = new ArrayList<String>(2);
        scopes.add("write");
        scopes.add("read");
        resource.setAccessTokenUri(bufferTokenUrl);
        resource.setClientId(bufferClientId);
        resource.setClientSecret(bufferClientSecret);
        resource.setGrantType("authorization_code");
        resource.setScope(scopes);

        resource.setUsername(bufferUsername);
        resource.setPassword(bufferPassword);

        return resource;
    }

    @Bean
    public OAuth2RestOperations restTemplate() {
        AccessTokenRequest atr = new DefaultAccessTokenRequest();

        return new OAuth2RestTemplate(resource(), new DefaultOAuth2ClientContext(atr));
    }
}
