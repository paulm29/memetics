package au.com.memetics.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Getter
@Setter
@ToString
@JsonPropertyOrder({
        "id",
        "createdDate",
        "nickname",
        "credits",
        "title",
        "caption",
        "url"
})
@ApiModel(value = "MemeExportDTO")
public class MemeExportDTO {
    private Long id;
    private String createdDate;
    private String nickname;
    private String credits;
    private String title;
    private String caption;
    private String url;
}
