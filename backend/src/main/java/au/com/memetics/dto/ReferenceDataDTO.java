package au.com.memetics.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ReferenceDataDTO {
    public List<ReferenceDataItem> wordpressCategories;
    public List<ReferenceDataItem> states;
    public List<ReferenceDataItem> countries;
}
