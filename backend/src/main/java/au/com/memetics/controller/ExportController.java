package au.com.memetics.controller;


import au.com.memetics.dto.MemeExportDTO;
import au.com.memetics.entity.CsvExport;
import au.com.memetics.entity.Meme;
import au.com.memetics.entity.MemeSearchCriteria;
import au.com.memetics.mapping.Mapper;
import au.com.memetics.service.MemeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(basePath = "export", value = "export", description = "Operations with exports", produces = "text/csv")
public class ExportController {
    private final Mapper mapper;
    private final MemeService service;

    public ExportController(final Mapper mapper, final MemeService memeService) {
        this.mapper = mapper;
        this.service = memeService;
    }

    @GetMapping(path = "/meme-export", produces = "text/csv")
    @ApiOperation(value = "Export memes to CSV file.")
    public CsvExport export(@ModelAttribute MemeSearchCriteria criteria) {
        List<Meme> memes = service.find(criteria);
        List<MemeExportDTO> exports = mapper.mapAsList(memes, MemeExportDTO.class);

        CsvExport csvExport = new CsvExport();
        csvExport.setFilename("results.csv");
        csvExport.setExports(exports);

        return csvExport;
    }
}
