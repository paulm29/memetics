package au.com.memetics.controller;

import au.com.memetics.controller.exception.NotFoundException;
import au.com.memetics.entity.Vote;
import au.com.memetics.service.VoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.CREATED;

@Api(tags = {"votes"}, basePath = "/votes", value = "votes",
        description = "Operations with votes", produces = "application/json")
@RestController
@RequestMapping("/rest/votes")
@Slf4j
public class VoteController {
    private final VoteService service;

    @Autowired
    public VoteController(final VoteService voteService) {
        this.service = voteService;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get vote by id")
    @ApiResponse(code = 404, message = "Not found")
    public Vote get(@PathVariable("id") long id) {
        Vote vote = service.get(id);
        if (isNull(vote)) {
            throw new NotFoundException();
        }
        return vote;
    }

    @GetMapping("/meme/{memeId}")
    @ApiOperation(value = "Get votes for meme")
    public List<Vote> getByMeme(@RequestParam("memeId") long id) {
        return service.getByMemeId(id);
    }

    @GetMapping("/profile/{id}")
    @ApiOperation(value = "Get votes for profile")
    public List<Vote> getByProfile(@RequestParam("profileId") long id) {
        return service.getByProfileId(id);
    }

    @PostMapping
    @ApiOperation(value = "Create meme")
    @ApiResponse(code = 201, message = "Created")
    @ResponseStatus(CREATED)
    public Vote create(@RequestBody Vote vote) {
        return service.create(vote);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update vote")
    @ApiResponse(code = 404, message = "Not found")
    public Vote update(@PathVariable("id") long id, @RequestBody Vote vote) {
        verifyExists(id);
        return service.update(vote);
    }

    private void verifyExists(final long id) {
        if (isNull(service.get(id))) {
            throw new NotFoundException();
        }
    }
}
