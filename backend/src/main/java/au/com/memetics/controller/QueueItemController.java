package au.com.memetics.controller;

import au.com.memetics.controller.exception.NotFoundException;
import au.com.memetics.entity.QueueItem;
import au.com.memetics.service.ProfileService;
import au.com.memetics.service.QueueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Api(tags = {"queue"}, basePath = "/profiles", value = "Profiles",
        description = "Manage queue", produces = "application/json")
@RestController
@RequestMapping("/")
@Slf4j
public class QueueItemController {
    private final ProfileService profileService;
    private final QueueService queueService;

    @Autowired
    public QueueItemController(final ProfileService profileService, QueueService queueService) {
        this.profileService = profileService;
        this.queueService = queueService;
    }

    @DeleteMapping("/rest/queue")
    @ApiOperation(value = "Get all queue items (queue) for a profile")
    public void deleteAllQueues() {
        queueService.deleteAllQueues();
    }

    @GetMapping("/rest/profiles/{profileId}/queue")
    @ApiOperation(value = "Get all queue items (queue) for a profile")
    public List<QueueItem> getQueue(@PathVariable("profileId") long profileId) {
        return queueService.getQueueForProfile(profileId);
    }

    @PostMapping("/rest/profiles/{profileId}/queue")
    @ApiOperation(value = "Create queue item.")
    @ResponseStatus(CREATED)
    public QueueItem createQueueItem(@PathVariable("profileId") long profileId, @RequestBody QueueItem queueItem) {
        return queueService.create(queueItem);
    }

    @PutMapping("/rest/profiles/{profileId}/queue/{queueId}")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "Update a QueueItem.")
    public QueueItem update(@PathVariable("profileId") long profileId, @PathVariable("queueId") long queueId, @RequestBody QueueItem queueItem) {
        verifyQueueExists(queueId);
        return queueService.update(queueItem);
    }

    @DeleteMapping("/rest/profiles/{profileId}/queue/{queueId}")
    @ApiOperation(value = "Delete queue item.")
    @ResponseStatus(NO_CONTENT)
    public void deleteQueueItem(@PathVariable("profileId") long profileId, @PathVariable("queueId") long queueId) {
        verifyQueueExists(queueId);
        queueService.delete(queueId);
    }

    private void verifyProfileExists(final long id) {
        if (isNull(profileService.get(id))) {
            throw new NotFoundException();
        }
    }

    private QueueItem verifyQueueExists(final long id) {
        QueueItem queueItem = queueService.get(id);
        if (Objects.isNull(queueItem)) {
            throw new NotFoundException();
        }
        return queueItem;
    }
}
