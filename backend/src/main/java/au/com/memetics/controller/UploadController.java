package au.com.memetics.controller;

import static org.springframework.http.HttpStatus.OK;

import au.com.memetics.service.ProfileService;
import au.com.memetics.service.UploadException;
import au.com.memetics.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;

import au.com.memetics.service.ProfileService;
import au.com.memetics.service.UploadException;
import au.com.memetics.service.UploadService;

@RestController
@Slf4j
@Api(tags = {"upload"}, basePath = "/", value = "upload",
        description = "Upload files", produces = "application/json")
public class UploadController {
    private final UploadService service;
    private final ProfileService profileService;

    @Autowired
    public UploadController(final UploadService uploadService, ProfileService profileService) {
        this.service = uploadService;
        this.profileService = profileService;
    }

    @PostMapping(path = "/upload")
    @ResponseStatus(OK)
    @ApiResponse(code = 200, message = "Success")
    @ApiOperation(value = "Upload file")
    public void upload(final @RequestParam("file") MultipartFile file, final @RequestParam("flowFilename") String filename, final @RequestParam("flowIdentifier") String tag) throws UploadException {
        service.upload(file, filename, tag, profileService.getProfileFromContext());
    }
}
