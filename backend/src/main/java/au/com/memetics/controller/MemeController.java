package au.com.memetics.controller;

import au.com.memetics.controller.exception.NotFoundException;
import au.com.memetics.entity.Comment;
import au.com.memetics.entity.Meme;
import au.com.memetics.entity.MemeSearchCriteria;
import au.com.memetics.entity.MemeSearchResults;
import au.com.memetics.mapping.Mapper;
import au.com.memetics.service.CommentService;
import au.com.memetics.service.MemeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Date;
import java.util.List;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Api(tags = {"memes"}, basePath = "/rest/memes", value = "Memes",
        description = "Operations with memes", produces = "application/json")
@RestController
@RequestMapping("/rest/memes")
@Slf4j
public class MemeController {
    private final Mapper mapper;
    private final MemeService service;
    private final CommentService commentService;

    @Autowired
    public MemeController(final Mapper mapper, final MemeService service, CommentService commentService) {
        this.mapper = mapper;
        this.service = service;
        this.commentService = commentService;
    }

    @GetMapping("/pages")
    @SuppressWarnings("unchecked")
    @ApiOperation(value = "Get all memes with pagination")
    public List<Meme> getAll(final @RequestParam("start") int start,
                             final @RequestParam("size") int size) {
        if (start >= 0 && size >= 0) {
            return service.getAllPagination(start, size);
        }
        return service.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get meme by id")
    @ApiResponse(code = 404, message = "Not found")
    public Meme getMeme(final @PathVariable("id") long id) {
        return verifyMemeExists(id);
    }

    @PostMapping
    @ApiOperation(value = "Create meme")
    @ApiResponse(code = 201, message = "Created")
    @ResponseStatus(CREATED)
    public Meme create(@RequestBody Meme meme) {
        return service.create(meme);
    }

    @PutMapping("/{id}")
    public Meme update(final @PathVariable("id") long id, @RequestBody Meme meme) {
        verifyMemeExists(id);
        return service.update(meme);
    }

    @DeleteMapping("/{id}")
    @RolesAllowed("ROLE_ADMIN")
    @ApiOperation(value = "Delete profile. Admin only to prevent abuse.")
    @ApiResponse(code = 404, message = "Not found")
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable("id") long id, @RequestParam(value = "deleteFromImgur", required = false) Boolean deleteFromImgur) {
        verifyMemeExists(id);
        service.delete(id, deleteFromImgur);
    }

    @GetMapping
    @ApiOperation(value = "Search for memes.")
    @SuppressWarnings("unchecked")
    public MemeSearchResults search(@ModelAttribute MemeSearchCriteria criteria) {
        return service.search(criteria);
    }

    @GetMapping("random")
    @ApiOperation(value = "Get a random meme")
    public Meme getRandomMeme() {
        return service.getRandomMeme();
    }

    @GetMapping("count")
    @ApiOperation(value = "Return total number of memes along with the time the count was taken.")
    public ResponseEntity<Object> getCount() {
        long count = service.getCount();
        Date countDate = new Date();

        HttpHeaders httpHeaders = new HttpHeaders();
//        "count", count

        return ResponseEntity.ok().headers(httpHeaders)
                .lastModified(countDate.getTime()).build();
    }

//    @GetMapping("/export")
////    @Produces("text/csv")
//    @ApiOperation(value = "Export memes to CSV file.")
//    @SuppressWarnings("unchecked")
//    public CsvExport export(@ModelAttribute MemeFilterBean memeFilterBean) {
//        List<Meme> memes = service.find(mapper.map(memeFilterBean, MemeSearchCriteria.class));
//        List<MemeExportDTO> exports = mapper.mapAsList(memes, MemeExportDTO.class);
//
//        CsvExport csvExport = new CsvExport();
//        csvExport.setFilename("results.csv");
//        csvExport.setExports(exports);
//
//        return csvExport;
//    }

    @GetMapping("/liked")
    @SuppressWarnings("unchecked")
    public List<Meme> getLiked(@RequestParam("profileId") final long profileId) {
        return service.getLiked(profileId);
    }

    @GetMapping("/unprocessed")
    @SuppressWarnings("unchecked")
    public List<Meme> getUnprocessed(@RequestParam("profileId") final long profileId) {
        return service.getUnprocessed(profileId);
    }

    @PutMapping("/{id}/increment-usage")
    @RolesAllowed("ROLE_ADMIN")
    @ApiOperation(value = "Increment usage. Used by admins for manual adjustments.")
    @ApiResponse(code = 404, message = "Not found")
    public void incrementUsage(final @PathVariable("id") long id, @RequestBody Meme meme) {
        verifyMemeExists(id);
        service.incrementUsage(id);
    }

    @PutMapping("/{id}/decrement-usage")
    @RolesAllowed("ROLE_ADMIN")
    @ApiOperation(value = "Decrement usage. Used by admin for manual adjustments.")
    @ApiResponse(code = 404, message = "Not found")
    public void decrementUsage(final @PathVariable("id") long id, @RequestBody Meme meme) {
        verifyMemeExists(id);
        service.decrementUsage(id);
    }

    private Meme verifyMemeExists(final long id) {
        Meme meme = service.get(id);
        if (isNull(meme)) {
            throw new NotFoundException();
        }
        return meme;
    }

    @GetMapping("{memeId}/comments")
    @ApiOperation(value = "Get comments for a meme.")
    @ApiResponse(code = 404, message = "Not found")
    public List<Comment> getCommentsForMeme(@PathVariable("memeId") long memeId) {
        return commentService.getCommentsForMeme(memeId);
    }

    @GetMapping("{memeId}/comments/{commentId}")
    @ApiOperation(value = "Get a particular comment for a meme.")
    @ApiResponse(code = 404, message = "Not found")
    public Comment get(@PathVariable("memeId") long memeId, @PathVariable("commentId") long commentId) {
        Comment comment = commentService.get(commentId);
        if (comment == null) {
            throw new NotFoundException();
        }
        return comment;
    }

    @PostMapping("{memeId}/comments")
    @ApiResponse(code = 201, message = "Created")
    @ApiOperation(value = "Create a new comment for a meme.")
    @ResponseStatus(CREATED)
    public Comment create(@PathVariable("memeId") long memeId, @RequestBody Comment comment) {
        return commentService.create(comment);
    }

    @PutMapping("{memeId}/comments/{commentId}")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "Update a comment for a meme.")
    public Comment update(@PathVariable("memeId") long memeId, @PathVariable("commentId") long commentId, @RequestBody Comment comment) {
        verifyCommentExists(commentId);
        return commentService.update(comment);
    }

    @DeleteMapping("{memeId}/comments/{commentId}")
    @ApiResponse(code = 204, message = "No content")
    @ApiOperation(value = "Delete a comment for a meme.")
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable("memeId") long memeId, @PathVariable("commentId") long commentId) {
        verifyCommentExists(commentId);
        commentService.delete(commentId);
    }

    private void verifyCommentExists(final long id) {
        if (isNull(commentService.get(id))) {
            throw new NotFoundException();
        }
    }

}
