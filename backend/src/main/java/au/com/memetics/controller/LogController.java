package au.com.memetics.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;

import au.com.memetics.entity.Log4JavaScriptEntry;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import au.com.memetics.entity.Log4JavaScriptEntry;

@RestController
@Slf4j
@Api(basePath = "/", value = "log", description = "Operations with log", produces = "application/json")
public class LogController {

    @PostMapping(value = "/log", consumes = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Post log entries")
    public void log(@RequestBody final List<Log4JavaScriptEntry> logEntries) {
        logEntries.forEach(this::log);
    }

    private void log(final Log4JavaScriptEntry logEntry) {
        String msg = logEntry.toString();
        switch (logEntry.getLevel()) {
            case DEBUG:
                log.debug(msg);
                break;
            case INFO:
                log.info(msg);
                break;
            case WARN:
                log.warn(msg);
                break;
            case ERROR:
                log.error(msg);
                break;
            default:
                log.debug(msg);
        }
    }
}
