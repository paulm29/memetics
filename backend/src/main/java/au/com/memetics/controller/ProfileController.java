package au.com.memetics.controller;

import au.com.memetics.controller.exception.NotFoundException;
import au.com.memetics.dto.ProfileDTO;
import au.com.memetics.entity.*;
import au.com.memetics.mapping.Mapper;
import au.com.memetics.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Objects;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@Api(tags = {"profiles"}, basePath = "/profiles", value = "Profiles",
        description = "Manage profiles", produces = "application/json")
@RestController
@RequestMapping("/rest/profiles")
@Slf4j
public class ProfileController {
    private final ProfileService profileService;
    private final StatsService statsService;
    private final FollowService followService;
    private final QueueService queueService;
    private final ScheduleService scheduleService;
    private final HashtagFavouriteService hashtagFavouriteService;
    private final CommentService commentService;

    @Autowired
    public ProfileController(final ProfileService profileService, StatsService statsService, FollowService followService, QueueService queueService, ScheduleService scheduleService, HashtagFavouriteService hashtagFavouriteService, Mapper mapper, CommentService commentService) {
        this.profileService = profileService;
        this.statsService = statsService;
        this.followService = followService;
        this.queueService = queueService;
        this.scheduleService = scheduleService;
        this.hashtagFavouriteService = hashtagFavouriteService;
        this.commentService = commentService;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get profile by id")
    @ApiResponse(code = 404, message = "Not found")
    public Profile get(final @PathVariable("id") long id) {
        Profile profile = profileService.get(id);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return profile;
    }

    @GetMapping("/comments")
    @ApiOperation(value = "Get all comments for all memes (memeId ignored). Optional filter on profile.")
    public List<Comment> getAllComments(@RequestParam(value = "profileId", required = false) Long profileId) {
        return commentService.getAll(profileId);
    }

    @GetMapping
    @RolesAllowed("ROLE_ADMIN")
    @ApiOperation(value = "Get all profiles. Admin only.")
    @ApiResponse(code = 404, message = "Not found")
    public List<Profile> getAll(@ModelAttribute ProfileSearchCriteria profileSearchCriteria) {
        return profileService.search(profileSearchCriteria);
    }

    @GetMapping("/nickname/{nickname}")
    @ApiOperation(value = "Get profile by nickname")
    @ApiResponse(code = 404, message = "Not found")
    public Profile getByNickname(final @PathVariable("nickname") String nickname) {
        Profile profile = profileService.getByNickname(nickname);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return profile;
    }

    @GetMapping("/email/{email}")
    @ApiOperation(value = "Get profile by email")
    @ApiResponse(code = 404, message = "Not found")
    public Profile getByEmail(final @PathVariable("email") String email) {
        Profile profile = profileService.getByEmail(email);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return profile;
    }

    @PostMapping
    @RolesAllowed("ROLE_ADMIN")
    @ApiOperation(value = "Create profile. Admin only.")
    @ApiResponse(code = 201, message = "Created")
    @ResponseStatus(CREATED)
    public Profile create(@RequestBody Profile profile) {
        return profileService.create(profile);
    }

    @PutMapping("/{id}")
    @PreAuthorize("@securityService.canAccess(#id)")
    @ApiOperation(value = "Update profile")
    @ApiResponse(code = 404, message = "Not found")
    public Profile update(final @PathVariable("id") long id, @RequestBody ProfileDTO profile) {
        verifyProfileExists(id);
        return profileService.update(profile);
    }

    @DeleteMapping("/{id}")
    @RolesAllowed("ROLE_ADMIN")
    @ApiOperation(value = "Delete profile. Admin only.")
    @ApiResponse(code = 404, message = "Not found")
    @ResponseStatus(NO_CONTENT)
    public void deleteProfile(final @PathVariable("id") long id) {
        verifyProfileExists(id);
        profileService.delete(id);
    }

    @GetMapping("/{id}/stats")
    @ApiOperation(value = "Get profile by id")
    @ApiResponse(code = 404, message = "Not found")
    public ProfileStats getStats(final @PathVariable("id") long id) {
        Profile profile = profileService.get(id);
        if (isNull(profile)) {
            throw new NotFoundException();
        }

        return profileService.getStats(profile.getId());
    }

    @GetMapping("/{id}/stats-retweets")
    @ApiOperation(value = "Get retweet stats by profile id")
    @ApiResponse(code = 404, message = "Not found")
    public StatsRetweets getStatsRetweets(final @PathVariable("id") long id, @ModelAttribute TweetSearchCriteria criteria) {
        Profile profile = profileService.get(id);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return statsService.getRetweets(criteria);
    }

    @PostMapping("/{id}/stats-retweets-publish")
    @ResponseStatus(CREATED)
    public Long publishRetweets(final @PathVariable("id") long id, @RequestBody TweetPost tweetPost) {
        Profile profile = profileService.get(id);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return statsService.publishRetweets(tweetPost);
    }

    @GetMapping("/{id}/stats-retweets-embed")
    @ApiOperation(value = "Get retweet stats by profile id")
    @ApiResponse(code = 404, message = "Not found")
    public StatsRetweets getStatsRetweetsAsEmbed(final @PathVariable("id") long id, @RequestParam("startDate") String startDate) {
        Profile profile = profileService.get(id);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return statsService.getRetweetsEmbed(startDate);
    }

    @GetMapping("/{id}/stats-tweets")
    @ApiOperation(value = "Get retweet stats by profile id")
    @ApiResponse(code = 404, message = "Not found")
    public StatsTweets getStatsTweets(final @PathVariable("id") long id, @RequestParam("startDate") String startDate, @RequestParam("maxRetweets") Integer maxRetweets, @RequestParam("maxLikes") Integer maxLikes) {
        Profile profile = profileService.get(id);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return statsService.getTweets(startDate, maxRetweets, maxLikes);
    }

    @GetMapping("/{profileId}/stats-tweets-memetics")
    @ApiOperation(value = "Get tweets by profile id")
    @ApiResponse(code = 404, message = "Not found")
    public List<TweetInfo> getTweets(final @PathVariable("profileId") long id) {
        Profile profile = profileService.get(id);
        if (isNull(profile)) {
            throw new NotFoundException();
        }
        return statsService.getTweetInfos(id);
    }

    @PostMapping("/{profileId}/follows")
    @ApiResponse(code = 201, message = "Created")
    @ApiOperation(value = "Create follow")
    @ResponseStatus(CREATED)
    public Follow create(@PathVariable("profileId") long profileId, @RequestBody Follow follow) {
        return followService.create(follow);
    }

    @DeleteMapping("/{profileId}/follows/{followId}")
    @ApiResponse(code = 204, message = "No content")
    @ApiOperation(value = "Delete follow")
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable("profileId") long profileId, @PathVariable("followId") long followId) {
        followService.delete(followId);
    }

    @PostMapping("/{profileId}/hashtag-favourites")
    @ApiResponse(code = 201, message = "Created")
    @ApiOperation(value = "Create hashtag favourite.")
    @ResponseStatus(CREATED)
    public HashtagFavourite createHashtagFavourite(@PathVariable("profileId") long profileId, @RequestBody HashtagFavourite hashtagFavourite) {
        return hashtagFavouriteService.createHashtagFavourite(hashtagFavourite);
    }

    @DeleteMapping("/{profileId}/hashtag-favourites/{hashtagFavouriteId}")
    @ApiResponse(code = 204, message = "No content")
    @ApiOperation(value = "Delete hashtag favourite.")
    @ResponseStatus(NO_CONTENT)
    public void deleteHashtagFavourite(@PathVariable("profileId") long profileId, @PathVariable("hashtagFavouriteId") long hashtagFavouriteId) {
        hashtagFavouriteService.deleteHashtagFavourite(hashtagFavouriteId);
    }

    @GetMapping("{profileId}/schedule")
    @ApiOperation(value = "Get schedule for a profile")
    public Schedule getSchedule(@PathVariable("profileId") long profileId) {
        Schedule schedule = scheduleService.findByProfileId(profileId);
        if (isNull(schedule)) {
            throw new NotFoundException();
        }
        return schedule;
    }

    @PostMapping("{profileId}/schedule")
    @ApiOperation(value = "Create schedule")
    @ResponseStatus(CREATED)
    public Schedule createSchedule(@PathVariable("profileId") long profileId, @RequestBody Schedule schedule) {
        return scheduleService.create(schedule);
    }

    @PutMapping("{profileId}/schedule/{scheduleId}")
    @ApiResponse(code = 200, message = "Ok")
    @ApiOperation(value = "Update a schedule.")
    public Schedule update(@PathVariable("profileId") long profileId, @PathVariable("scheduleId") long scheduleId, final Schedule schedule) {
        verifyScheduleExists(scheduleId);
        return scheduleService.update(schedule);
    }

    private void verifyProfileExists(final long id) {
        if (isNull(profileService.get(id))) {
            throw new NotFoundException();
        }
    }

    private void verifyScheduleExists(final long id) {
        if (Objects.isNull(scheduleService.get(id))) {
            throw new NotFoundException();
        }
    }
}
