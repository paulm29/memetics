package au.com.memetics.controller;

import au.com.memetics.dto.BufferRetweetDTO;
import au.com.memetics.entity.BufferUpdate;
import au.com.memetics.service.BufferService;
import com.bufferj.entity.Update;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;

@Api(tags = {"wordpress"}, basePath = "/rest", value = "Wordpress posts",
        description = "Operations with buffer", produces = "application/json")
@RestController
@RequestMapping("/rest")
@Slf4j
public class BufferController {
    private final BufferService bufferService;

    public BufferController(BufferService bufferService) {
        this.bufferService = bufferService;
    }

    @PostMapping("/buffer-retweet")
    @ResponseStatus(CREATED)
    public List<Update> retweetCreate(@RequestBody BufferRetweetDTO dto) {
        return bufferService.retweetCreate(dto);
    }

    @PostMapping("/buffer-update")
    @ResponseStatus(CREATED)
    public List<Update> bufferUpdateCreate(@RequestBody BufferUpdate bufferUpdate) {
        return bufferService.bufferUpdateCreate(bufferUpdate);
    }
}
