package au.com.memetics.controller;

import au.com.memetics.entity.BackupResult;
import au.com.memetics.service.BackupService;
import au.com.memetics.service.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;


@Api(tags = {"admins"}, basePath = "/admins", value = "Admins",
        description = "Admin resources", produces = "application/json")
@Slf4j
@RestController
@RequestMapping("/rest/admins")
public class AdminController {
    private final ProfileService service;
    private final BackupService backupService;

    @Autowired
    public AdminController(final ProfileService profileService, BackupService backupService) {
        this.service = profileService;
        this.backupService = backupService;
    }

    @PostMapping("backup")
    @RolesAllowed("ROLE_ADMIN")
    @ApiOperation(value = "Trigger backup of images. Admin only.")
    public BackupResult backupImages() {
        return backupService.backupImages();
    }

    @Value("${api.version}")
    private String version;

    @GetMapping("version")
    @ApiOperation(value = "Gets version of API")
    public String getVersion() {
        return version;
    }
}
