package au.com.memetics.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@Slf4j
@ApiIgnore
public class IndexController {
    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/swag")
    public String home() {
        return "redirect:swagger-ui.html";
    }

    @RequestMapping(path = {"/profile/**", "/login/**", "/registration/**", "/admin/**", "favicon.ico"})
    public String forwardToApp() {
        return "forward:/index.html";
    }
}
