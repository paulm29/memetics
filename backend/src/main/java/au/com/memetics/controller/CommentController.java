package au.com.memetics.controller;

import au.com.memetics.entity.Comment;
import au.com.memetics.service.CommentService;
import au.com.memetics.service.MemeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = {"memes"}, basePath = "/rest/comments", value = "Memes",
        description = "Operations with memes", produces = "application/json")
@RestController
@RequestMapping("/rest/comments")
@Slf4j
public class CommentController {
    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }
}
