import "jquery";
import "angular";
import "angular-messages";
import "angular-ui-router";
import "angular-ui-router-title";
import "bootstrap";
import "angular-ui-bootstrap";
import "angular-tablesort";
import "angular-material";
import "angular-tag-cloud";

require("angular-material/angular-material.min.css");
require("angular-tablesort/tablesort.css");
require("angular-tag-cloud/src/css/ng-tag-cloud.css");
require("bootstrap/dist/css/bootstrap.css");
require("bootstrap-social/bootstrap-social.css");
require("font-awesome/css/font-awesome.css");
require("./app/common/app.css");
require("./app/alert/alert.css");

import "./ng-imgur";

import {authEvents} from "./config/auth.events";
import {roles} from "./config/roles";
import {application} from "./config/application";
import {route} from "./config/route";
import {angularConfig} from "./config/angular.config";
import {logging} from "./config/logging";
import {http} from "./config/http";
import {idle} from "./config/idle";
import {requireAll} from "./app/common/require.all";
import {autoTemplateCache} from "./config/auto.template.cache.js";
const htmlRequireContext = require.context("./app", true, /.*\.html/);

angular.module("memetics", ["ui.router", "ui.router.title", "ui.bootstrap", "ngMessages", "ngAria", "ngMaterial", "ngImgur", "tableSort", "ngTagCloud", "ngSanitize", "ngIdle", "ui.scroll", "flow", "flow.provider", "dndLists", "ngCsv"])
    .constant("USER_ROLES", roles)
    .constant("AUTH_EVENTS", authEvents)
    .constant("APPLICATION", application)
    .config(angularConfig)
    .config(http)
    .config(idle)
    .config(route)
    .config(logging)
    .config(["flowFactoryProvider", function (flowFactoryProvider) {
        flowFactoryProvider.factory = function (opts) { // https://github.com/flowjs/ng-flow/issues/92
            const Flow = require("ng-flow/dist/ng-flow-standalone.js");
            return new Flow(opts);
        };
    }]);

angular.module("memetics").run(["Idle", function (Idle) {
    Idle.watch();
}]);

angular.module("memetics").run(autoTemplateCache(htmlRequireContext, "app/"));

requireAll(require.context("./app", true, /^(?!.*spec\.js).*\.js$/));