# memetics
Memetics is the shitposter's best friends. It helps you organise memes for posting to social media. 
Only Twitter is supported at this time.

Memetics is a AngularJS web application with a Spring backend

# Setup

Requirements:
- Java 8
- [Maven 3.3](https://archive.apache.org/dist/maven/maven-3/3.3.3/binaries/)
- MySQL
- Node

Development steps:

0. Change database connection settings in pom.xml, especially mysql.memetics.svc
1. npm install
2. mvn package
3. set up the memetics-local run configuration in IDEA and run
4. For e2e tests, you many need to update protractor: `npm update protractor -g`

## IntelliJ Development Setup

Enable JSHint and configure to use `.jshintrc`.

### Run configurations:

- build: npm configuration, set scripts = `build`
- e2e: npm run local
- karma: Karma configuration, select the karma.conf.js from select. build configuration should run first
- memetics-local: Tomcat configuration with following:
    - add build to the `Before launch` 
    - Deployments: `memetics:war exploded`
    - On frame deactivation: update `update classes and resources`
- test: Junit configuration: run all in package au.com.memetics

Development cycle for front-end changes when app deployed through intellij is: make your change -> `build` -> refresh page

## Hot tips

- To deploy just front end changes manually: `npm run build:dev`
- You can also use `npm run build-watch` and rely on webpack automatically creating the bundle.