package com.bufferj.client.util;

import com.bufferj.entity.Profile;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class CreateUpdates {
    private List<Profile> profiles;
    private String text;
    private Boolean shorten;
    private Boolean now;
    private Boolean top;
    private String mediaLink;
    private String mediaDescription;
    private String mediaTitle;
    private String mediaPicture;
    private String mediaThumbnail;
    private Boolean attachment;
    private Long scheduledAt;
    private String mediaPhoto;
    private Map<String, String> retweet;

    public CreateUpdates() {
        this.profiles = new ArrayList<>();
    }

    public List<Profile> getProfiles() {
        return profiles;
    }

    public void addProfile(Profile profile) {
        if (profiles == null) {
            profiles = new ArrayList<>();
        }

        profiles.add(profile);
    }
}
